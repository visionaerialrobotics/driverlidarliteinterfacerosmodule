//////////////////////////////////////////////////////
//  px4flow_interface_ROSModule_okto.h
//
//  Created on: Jan 8, 2015
//      Author: Hriday Bavle
//
//
//  Last modification on: Jan 8 2015
//      Author: Hriday Bavle
//
//////////////////////////////////////////////////////


#ifndef _PELICAN_OUTS_H
#define _PELICAN_OUTS_H


#include "droneModuleROS.h"
#include "communication_definition.h"
//Altitude and Ground Speed
#include "droneMsgsROS/droneAltitude.h"
#include "droneMsgsROS/vector2Stamped.h"

//// ROS  ///////
#include "ros/ros.h"
#include "std_msgs/Float64.h"

//I/O stream
//std::cout
#include <iostream>

//Math
//M_PI
#include <cmath>

// low pass filter
#include "control/LowPassFilter.h"
#include "xmlfilereader.h"
#include "control/filtered_derivative_wcb.h"

#define PX4FLOW_INT_FDWCB_PRE_TR       (  0.020                            )
#define PX4FLOW_INT_FDWCB_POST_TR      (  0.020                            )
#define PX4FLOW_INT_FDWCB_TDERIV       (  0.200                            )
#define PX4FLOW_INT_FDWCB_TMEMORY      (  5.000 * PX4FLOW_INT_FDWCB_TDERIV )
#define PX4FLOW_INT_FDWCB_SENSORFREQ   (100.000                            )

/////////////////////////////////////////
// Class Altitude
//
//   Description
//
/////////////////////////////////////////
class AltitudeROSModule : public DroneModule
{

    //Publisher
protected:
    ros::Publisher AltitudePubl;
    bool publishAltitudeValue();
    CVG_BlockDiagram::LowPassFilter h_lowpassfilter;


    //Subscriber
protected:
    CVG_BlockDiagram::FilteredDerivativeWCB filtered_derivative_wcb;
    ros::Subscriber AltitudeSubs;
    void altitudeCallback(const std_msgs::Float64::ConstPtr& msg);


    //Altitude msgs
protected:

    droneMsgsROS::droneAltitude AltitudeMsgs;


    //Constructors and destructors
public:
    AltitudeROSModule();
    ~AltitudeROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};

#endif
